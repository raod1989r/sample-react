import React, {Component} from 'react';

export default class NewPage extends React.Component {
    render () {
        return (
            <div>
                <input
                    type='text'
                    value={this.props.value}
                    onChange={this.props.onChange}
            />
    </div>
    )
    }
}